#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
//
#include "timers.h"
#include "floorf.h"
//
#define NA 1000000
#define NI 10
//
// forward declarations
//
void marla_floorf_vec( float* __restrict__ y, const float* __restrict__ x, int N )
{
        for (int ii = 0; ii < N; ++ii)
                y[ii] = marla_floorf(x[ii]);
}
 
//
//
//
int main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	float* __restrict__ x  = (float*) malloc(sizeof(float)*N);
	float* __restrict__ y  = (float*) malloc(sizeof(float)*N);
	float* __restrict__ z  = (float*) malloc(sizeof(float)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = 1e3*((float)rand() / (float)RAND_MAX - .5);
	}	
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_floorf_vec( y, x, N );
        c1 = rdtscp();
	printf("marla floor : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
		z[ii] = floor(x[ii]);
        c1 = rdtscp();
	//
        printf("lib   floor : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	//
#if 1
	for (int ii = 0; ii < 10; ++ii)
        {
		//if (y[ii] != floor(x[ii]))
			printf("%d: x = %.15f, floor = %.15f (%.15f), error = %.15f\n", ii, x[ii], y[ii], z[ii], fabs(y[ii] - z[ii]));
        }	
#endif
	//
	free(x);
	free(y);
	//
}

