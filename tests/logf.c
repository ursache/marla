#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
//
#include "timers.h"
#include "logf.h"
#ifdef __WITH_MPFR
#include "mpfr_check.h"
#endif
//
#define NA 1000000
#define NI 10
//
void marla_logf_vec( float* __restrict__ y, const float* __restrict__ x, int N )
{
        for (int ii = 0; ii < N; ++ii)
	{
                 y[ii] = marla_logf(x[ii]);
	}
}
//
int main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	float* __restrict__ x  = (float*) malloc(sizeof(float)*N);
	float* __restrict__ y  = (float*) malloc(sizeof(float)*N);
	float* __restrict__ z  = (float*) malloc(sizeof(float)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = (float)rand() / (float)RAND_MAX;
	}	
	//
	long c0, c1;
	//	
	//
        c0 = rdtscp();
	marla_logf_vec( y, x, N );
        c1 = rdtscp();
	printf("marla log : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
	{
		z[ii] = logf(x[ii]); 
	}	
        c1 = rdtscp();
	//
        printf("       log : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
#ifdef __WITH_MPFR	//
	mpfr_checkf(N, x, y, &mpfr_log);	
#else
	for (int ii = 0; ii < 10; ++ii)
        {
		if (y[ii] != logf(x[ii]))
			printf("%d: x = %.15f, log = %.15f (%.15f), error = %.15f\n", ii, x[ii], y[ii], log(x[ii]), fabs(y[ii] - log(x[ii])));
        }	
#endif
	//
	free(x);
	free(y);
	//
}

