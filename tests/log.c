#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
//
#ifdef __WITH_MPFR
#include <mpfr.h>
#include <gmp.h>
#include "mpfr_check.h"
#endif
//
#include "timers.h"
#include "log.h"
#ifdef __WITH_MPFR
#include "mpfr_check.h"
#endif
//
#define NA 1000000
#define NI 10
//
void marla_log_vec( double* __restrict__ y, const double* __restrict__ x, int N )
{
        for (int ii = 0; ii < N; ++ii)
	{
                 y[ii] = marla_log(x[ii]);
	}
}
//
//
//
int main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	double* __restrict__ x  = (double*) malloc(sizeof(double)*N);
	double* __restrict__ y  = (double*) malloc(sizeof(double)*N);
	double* __restrict__ z  = (double*) malloc(sizeof(double)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = (double)rand() / (double)RAND_MAX;
	}	
	//
	long c0, c1;
	//	
	//
	c0 = rdtscp();
	marla_log_vec( y, x, N );
	c1 = rdtscp();
	long long cm = (c1 - c0)/N;
	printf("marla log : %ld cycles\n", cm); fflush(stdout);
	//
	c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
		z[ii] = log(x[ii]);	
	c1 = rdtscp();
	printf("      log : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	//
#ifdef __WITH_MPFR
	mpfr_check(N, x, y, &mpfr_log);
	//
#else
	for (int ii = 0; ii < 10; ++ii)
	{
		printf("%d: x = %.15f, log = %.15f (%.15f), error = %.15f\n", ii, x[ii], y[ii], z[ii], fabs(y[ii] - z[ii]));
	}	
#endif
	//
	free(x);
	free(y);
	free(z);
	//
}

