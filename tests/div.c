#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
//
#include "timers.h"
#include "div.h"
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
void marla_div_vec(double*       __restrict__ z,
		   const double* __restrict__ x,
		   const double* __restrict__ y,
		   const int                  n)
{
    for(int i = 0; i < n; ++i)
        z[i] = marla_div(x[i], y[i]);
}
//
void marla_div_nr1_vec(double*   __restrict__ z,
                   const double* __restrict__ x,
		   const double* __restrict__ y,
                   const int                  n)
{
    for(int i = 0; i < n; ++i)
        z[i] = marla_div_nr1(x[i], y[i]);
}
//
void marla_div_nr2_vec(double*   __restrict__ z,
                   const double* __restrict__ x,
		   const double* __restrict__ y,
                   const int                  n)
{
    for(int i = 0; i < n; ++i)
        z[i] = marla_div_nr2(x[i], y[i]);
}
//
void marla_div_nr3_vec(double*   __restrict__ z,
                   const double* __restrict__ x,
		   const double* __restrict__ y,
                   const int                  n)
{
    for(int i = 0; i < n; ++i)
        z[i] = marla_div_nr3(x[i], y[i]);
}
//
void marla_div_nr4_vec(double*   __restrict__ z,
                   const double* __restrict__ x,
		   const double* __restrict__ y,
                   const int                  n)
{
    for(int i = 0; i < n; ++i)
        z[i] = marla_div_nr4(x[i], y[i]);
}
//
int  main()
{
        const int N     = NA;
        const int NITER = NI;
        //
        double* __restrict__ x   = (double*) malloc(sizeof(double)*N);
        double* __restrict__ y   = (double*) malloc(sizeof(double)*N);
        double* __restrict__ z   = (double*) malloc(sizeof(double)*N);
        double* __restrict__ z1  = (double*) malloc(sizeof(double)*N);
        double* __restrict__ z2  = (double*) malloc(sizeof(double)*N);
        //
        srand(time(NULL));
        //
        for (int ii = 0; ii < N; ++ii)
        {
                x[ii] = 10.*((double)rand() / (double)RAND_MAX);
                y[ii] = 10.*((double)rand() / (double)RAND_MAX);
        }
        //
        long c0, c1;
        //
        // benchmarking
        //
        c0 = rdtscp();
        marla_div_vec( z, x, y, N );
        c1 = rdtscp();
        printf("div     %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
        marla_div_nr1_vec( z1, x, y, N );
        c1 = rdtscp();
        printf("div RN1 %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
        marla_div_nr2_vec( z2, x, y, N );
        c1 = rdtscp();
        printf("div RN2 %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
#if DEBUG
        for (int ii = 0; ii < 10; ++ii)
        {
                //if (y[ii] != exp(x[ii]))
		printf("%d: %.15f/%.15f = %.15f %.15f, error = %.15f %.15f\n", ii, x[ii], y[ii], z1[ii], z2[ii], fabs(z2[ii] - x[ii]/y[ii]), 
				fabs(z1[ii] - x[ii]/y[ii]),  
				fabs(z2[ii] - x[ii]/y[ii])
		      );
        }
#endif
        //
        free(x);
        free(y);
        free(z);
        free(z1);
        free(z2);
        //
}

