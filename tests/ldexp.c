#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
//
#include "timers.h"
#include "ldexp.h"
//
#define NA 1000000
#define NI 10
//
//
//
void marla_ldexp_vec( double* __restrict__ y, const double* __restrict__ x, int* __restrict__ n, int N )
{
        for (int ii = 0; ii < N; ++ii)
	{
                //y[ii] = ldexp_vec_old(&x[ii], &z[ii]);
                //printf("op = %.15f, e = %d, %f %f\n", x[ii], n[ii], ldexp_vec_old(&x[ii], &n[ii]), ldexp_vec(&x[ii], &n[ii]));
                y[ii] = marla_ldexp(x[ii], n[ii]);
		//printf("---\n");
	}
}
//double ldexp( int* ppw2, double* px)
//
//
int main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	double* __restrict__ x  = (double*) malloc(sizeof(double)*N);
	double* __restrict__ y  = (double*) malloc(sizeof(double)*N);
	double* __restrict__ z  = (double*) malloc(sizeof(double)*N);
	int*    __restrict__ n  = (int*)    malloc(sizeof(int)   *N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = 100*(double)rand() / (double)RAND_MAX;
		n[ii] = (int) (rand()%10) - 5;
	}	
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_ldexp_vec( y, x, n, N );
        c1 = rdtscp();
	printf("marla ldexp : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
		z[ii] = ldexp(x[ii], n[ii]);
	c1 = rdtscp();
	printf("       ldexp : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
#if 1
	for (int ii = 0; ii < 10; ++ii)
        {
		//if (y[ii] != exp(x[ii]))
		printf("%d: x = %.15f, pow = %d, val = %.15f (%.15f), x*2^n = %.15f, error = %.15f\n", ii, x[ii], n[ii], y[ii], z[ii], x[ii]*pow(2, n[ii]), fabs(y[ii] - z[ii]));
        }	
#endif
	//
	free(x);
	free(y);
	free(z);
	//
}

