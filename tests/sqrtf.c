#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
//
#include "timers.h"
#include "sqrtf.h"
#ifdef __WITH_MPFR
#include "mpfr_check.h"
#endif
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
void marla_sqrt_vec( float* __restrict__ y, const float* __restrict__ x, int N )
{
        for (int ii = 0; ii < N; ++ii)
                y[ii] = marla_sqrtf(x[ii]);
}
 
//
//
//
int main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	float* __restrict__ x  = (float*) malloc(sizeof(float)*N);
	float* __restrict__ y  = (float*) malloc(sizeof(float)*N);
	float* __restrict__ z  = (float*) malloc(sizeof(float)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = 1000.*((float)rand() / (float)RAND_MAX);
		if (x[ii] < 100.) x[ii] = 0.0;
	}	
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_sqrt_vec( y, x, N );
        c1 = rdtscp();
	printf("marla sqrt : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
		z[ii] = sqrtf(x[ii]);
        c1 = rdtscp();
        printf("lib    sqrt : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	//
#ifdef __WITH_MPFR
	mpfr_checkf(N, x, y, mpfr_sqrt);
#else
	for (int ii = 0; ii < 10; ++ii)
        {
		//if (y[ii] != exp(x[ii]))
			printf("%d: x = %.15f, exp = %.15f (%.15f), error = %.15f\n", ii, x[ii], y[ii], z[ii], fabs(y[ii] - z[ii]));
        }	
#endif
	//
	free(x);
	free(y);
	//
}

