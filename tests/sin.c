#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <float.h>
#include <stdint.h>
#include <time.h>
#ifdef __WITH_MPFR
#include <mpfr.h>
#include <gmp.h>
#include "mpfr_check.h"
#endif
//
#include "timers.h"
#include "utils.h"
#include "sin.h"
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
void marla_sin_vec( double* __restrict__ y, const double* __restrict__ x, int N )
{
        for (int ii = 0; ii < NA; ++ii)
                y[ii] = marla_sin(x[ii]);
}
 
//
//
//
int main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	double* __restrict__ x    = (double*) malloc(sizeof(double)*N);
	double* __restrict__ y    = (double*) malloc(sizeof(double)*N);
	double* __restrict__ z    = (double*) malloc(sizeof(double)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = 100.*((double)rand() / (double)RAND_MAX - .5);
	}	
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_sin_vec( y, x, N );
        c1 = rdtscp();
	long long cm = (c1 - c0)/N;
	printf("marla sin : %ld cycles\n", cm); fflush(stdout);
        //
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
	{
		z[ii] = sin(x[ii]);
	}
        c1 = rdtscp();
        printf("lib   sin : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	//
#ifdef __WITH_MPFR
	mpfr_check(N, x, y, &mpfr_sin);
#else
	for (int ii = 0; ii < 10; ++ii)
        {
			printf("%d: x = %.15f, sin = %.15f (%.15f), error = ", ii, x[ii], y[ii], z[ii], fabs(y[ii] - z[ii]));
//#ifdef __WITH_MPFR
//			mpfr_out_str (stdout, 10, 0, ymp[ii], MPFR_RNDN);
//#else
			printf("%.15f", fabs(y[ii] - z[ii]));
//#endif
			printf("\n");	
	 
	}
#endif
	//
	free(x);
	free(y);
	//
}

