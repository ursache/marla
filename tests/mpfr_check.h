#pragma once

#ifdef __WITH_MPFR
#include "utils.h"

void mpfr_check(const unsigned int N, const double* __restrict__ x, const double * __restrict__ y, int (*f) (mpfr_t*, mpfr_t*, mpfr_rnd_t))
{
        mpfr_t* __restrict__ xmp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        mpfr_t* __restrict__ ymp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        mpfr_t* __restrict__ zmp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        //
        for (int ii = 0; ii < N; ++ii)
        {
                //
                mpfr_init2(xmp[ii], 80);
                mpfr_set_d(xmp[ii], x[ii], MPFR_RNDN);
                mpfr_init2(zmp[ii], 80);
                f(zmp + ii, xmp + ii, MPFR_RNDN);
                mpfr_init2(ymp[ii], 80);
                mpfr_set_d(ymp[ii], y[ii], MPFR_RNDN);
                //
        }
	//
        mpfr_t max_ulp;
        mpfr_init2(max_ulp, 80);
        computeULP(max_ulp, y, (const mpfr_t*) zmp, N);
	//
	mpfr_t max_rel_error;
        mpfr_init2(max_rel_error, 80);
        mpfr_t stdev;
        mpfr_init2(stdev, 80);
        computeRelativeError(&max_rel_error, y, (const mpfr_t*) zmp, N);
        printf("max ULP = "); mpfr_out_str(stdout, 10, 0, max_ulp, MPFR_RNDN); printf(", "); fflush(stdout);
        printf("max relative error = "); mpfr_out_str(stdout, 10, 0, max_rel_error, MPFR_RNDN); printf(" ");
	//
	mpfr_t dbl_eps ;
        mpfr_init2(dbl_eps, 80);
        mpfr_set_d(dbl_eps, __DBL_EPSILON__, MPFR_RNDN);
        if (mpfr_greater_p(max_rel_error, dbl_eps)) printf("%s[Failed]", "\x1B[31m");
        else printf("%s[Passed]", "\x1B[32m");
        printf("%s\n", "\x1B[0m");
	//
	free(xmp);
        free(ymp);
        free(zmp);
}
//
void mpfr_check2(const unsigned int N, const double* __restrict__ x, const double * __restrict__ y, const double* __restrict__ z, int (*f) (mpfr_t*, mpfr_t*, mpfr_t*, mpfr_rnd_t))
{
        mpfr_t* __restrict__ xmp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        mpfr_t* __restrict__ ymp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        mpfr_t* __restrict__ zmp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        mpfr_t* __restrict__ rmp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        //
        for (int ii = 0; ii < N; ++ii)
        {
                //
                mpfr_init2(xmp[ii], 80);
                mpfr_set_d(xmp[ii], x[ii], MPFR_RNDN);
                mpfr_init2(ymp[ii], 80);
                mpfr_set_d(ymp[ii], y[ii], MPFR_RNDN);
		mpfr_init2(zmp[ii], 80);
                mpfr_set_d(zmp[ii], z[ii], MPFR_RNDN);
                mpfr_init2(rmp[ii], 80);
                f(rmp + ii, ymp + ii, xmp + ii, MPFR_RNDN);
                //
        }
        //
        mpfr_t max_ulp;
        mpfr_init2(max_ulp, 80);
        computeULP(max_ulp, y, (const mpfr_t*) zmp, N);
        //
        mpfr_t max_rel_error;
        mpfr_init2(max_rel_error, 80);
        mpfr_t stdev;
        mpfr_init2(stdev, 80);
        computeRelativeError(&max_rel_error, y, (const mpfr_t*) zmp, N);
        printf("max ULP = "); mpfr_out_str(stdout, 10, 0, max_ulp, MPFR_RNDN); printf(", "); fflush(stdout);
        printf("max relative error = "); mpfr_out_str(stdout, 10, 0, max_rel_error, MPFR_RNDN); printf(" ");
        //
        mpfr_t dbl_eps;
        mpfr_init2(dbl_eps, 80);
        mpfr_set_d(dbl_eps, __DBL_EPSILON__, MPFR_RNDN);
        if (mpfr_greater_p(max_rel_error, dbl_eps)) printf("%s[Failed]", "\x1B[31m");
        else printf("%s[Passed]", "\x1B[32m");
        printf("%s\n", "\x1B[0m");
	//
        free(xmp);
        free(ymp);
        free(zmp);
}
//
void mpfr_checkf(const unsigned int N, const float* __restrict__ x, const float* __restrict__ y, int (*f) (mpfr_t*, mpfr_t*, mpfr_rnd_t))
{
        mpfr_t* __restrict__ xmp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        mpfr_t* __restrict__ ymp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        mpfr_t* __restrict__ zmp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        //
        for (int ii = 0; ii < N; ++ii)
        {
                //
                mpfr_init2(xmp[ii], 48);
                mpfr_set_d(xmp[ii], x[ii], MPFR_RNDN);
                mpfr_init2(zmp[ii], 48);
                f(zmp + ii, xmp + ii, MPFR_RNDN);
                mpfr_init2(ymp[ii], 48);
                mpfr_set_d(ymp[ii], y[ii], MPFR_RNDN);
                //
        }
        //
        mpfr_t max_ulp;
        mpfr_init2(max_ulp, 48);
        computeULPf(max_ulp, y, (const mpfr_t*) zmp, N);
        //
        mpfr_t max_rel_error;
        mpfr_init2(max_rel_error, 48);
        mpfr_t stdev;
        mpfr_init2(stdev, 48);
        computeRelativeErrorf(&max_rel_error, y, (const mpfr_t*) zmp, N);
        printf("max ULP = "); mpfr_out_str(stdout, 10, 0, max_ulp, MPFR_RNDN); printf(", "); fflush(stdout);
        printf("max relative error = "); mpfr_out_str(stdout, 10, 0, max_rel_error, MPFR_RNDN); printf(" ");
        //
        mpfr_t flt_eps ;
        mpfr_init2(flt_eps, 48);
        mpfr_set_d(flt_eps, __FLT_EPSILON__, MPFR_RNDN);
        if (mpfr_greater_p(max_rel_error, flt_eps)) printf("%s[Failed]", "\x1B[31m");
        else printf("%s[Passed]", "\x1B[32m");
        printf("%s\n", "\x1B[0m");
        //
        free(xmp);
        free(ymp);
        free(zmp);
}
//
void mpfr_checkf2(const unsigned int N, const float* __restrict__ x, const float* __restrict__ y, int (*f) (mpfr_t*, mpfr_t*, mpfr_rnd_t))
{
        mpfr_t* __restrict__ xmp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        mpfr_t* __restrict__ ymp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        mpfr_t* __restrict__ zmp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        //
        for (int ii = 0; ii < N; ++ii)
        {
                //
                mpfr_init2(xmp[ii], 48);
                mpfr_set_d(xmp[ii], x[ii], MPFR_RNDN);
                mpfr_init2(zmp[ii], 48);
                f(zmp + ii, xmp + ii, MPFR_RNDN);
                mpfr_init2(ymp[ii], 48);
                mpfr_set_d(ymp[ii], y[ii], MPFR_RNDN);
                //
        }
        //
        mpfr_t max_ulp;
        mpfr_init2(max_ulp, 48);
        computeULPf(max_ulp, y, (const mpfr_t*) zmp, N);
        //
        mpfr_t max_rel_error;
        mpfr_init2(max_rel_error, 48);
        mpfr_t stdev;
        mpfr_init2(stdev, 48);
        computeRelativeErrorf(&max_rel_error, y, (const mpfr_t*) zmp, N);
        printf("max ULP = "); mpfr_out_str(stdout, 10, 0, max_ulp, MPFR_RNDN); printf(", "); fflush(stdout);
        printf("max relative error = "); mpfr_out_str(stdout, 10, 0, max_rel_error, MPFR_RNDN); printf(" ");
        //
        mpfr_t flt_eps ;
        mpfr_init2(flt_eps, 48);
        mpfr_set_d(flt_eps, __FLT_EPSILON__, MPFR_RNDN);
        if (mpfr_greater_p(max_rel_error, flt_eps)) printf("%s[Failed]", "\x1B[31m");
        else printf("%s[Passed]", "\x1B[32m");
        printf("%s\n", "\x1B[0m");
        //
        free(xmp);
        free(ymp);
        free(zmp);
}
#endif
