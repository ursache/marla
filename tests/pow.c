#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <float.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#ifdef __WITH_MPFR
#include <gmp.h>
#include <mpfr.h>
#include "mpfr_check.h"
#endif
//
#include "timers.h"
#include "utils.h"
#include "pow.h"
//
#define NA 10000000
#define NI 10
//
#ifdef __riscv
#warning "riscv"
#endif
//
// forward declarations
//
void marla_pow_vec( double* __restrict__ z, const double* __restrict__ y, const double* __restrict__ x, int N )
{
        for (int ii = 0; ii < N; ++ii)
                z[ii] = marla_pow(x[ii], y[ii]);
}
//
//
//
int main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	double* __restrict__ x    = (double*) malloc(sizeof(double)*N);
	double* __restrict__ y    = (double*) malloc(sizeof(double)*N);
	double* __restrict__ z    = (double*) malloc(sizeof(double)*N);
	double* __restrict__ zz    = (double*) malloc(sizeof(double)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = fabs(((double)rand() / (double)RAND_MAX - .5));
		y[ii] = ((double)rand() / (double)RAND_MAX - .5);
	}	
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_pow_vec( z, y, x, N );
	//sleep(10);
        c1 = rdtscp();
	long long cm = (c1 - c0)/N;
	printf("marla pow : %ld cycles\n", cm); fflush(stdout);
        //
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
	{
		zz[ii] = pow(x[ii], y[ii]);
	}
        c1 = rdtscp();
        printf("lib   pow : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	//
#ifdef __WITH_MPFR
	mpfr_check2(N, y, x, z, &mpfr_pow);
#else
	for (int ii = 0; ii < 10; ++ii)
	{
		printf("%d: x = %.15f, y = %.15f, pow = %.15f (%.15f), error = ", ii, x[ii], y[ii], z[ii], zz[ii]);
		//#ifdef __WITH_MPFR
		//			mpfr_out_str (stdout, 10, 0, ymp[ii], MPFR_RNDN);
		//#else
		printf("%.15f", (zz[ii] - z[ii]));
		//#endif
		printf("\n");	

	}
#endif
	//
	free(x);
	free(y);
	free(z);
	free(zz);
	//
}

