//#include <immintrin.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cpuid.h>
//#include <immintrin.h>
#include <stdio.h>
#ifdef __WITH_MPFR
#include <gmp.h>
#include <mpfr.h>
#include "mpfr_check.h"
#endif
//
#include <stdint.h>
#include "timers.h"
#include "cexp.h"
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
#pragma omp declare simd uniform(x) aligned(x:32) uniform(y) aligned(y:32)
void marla_cexp_vec( double complex* __restrict__ y, const double complex* __restrict__ x, int N )
{
        for (int ii = 0; ii < N; ++ii)
                y[ii] = marla_cexp(x[ii]);
}
//
int main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	double complex* __restrict__ x    = (double complex*) malloc(sizeof(double complex)*N);
	double complex* __restrict__ y    = (double complex*) malloc(sizeof(double complex)*N);
	double complex* __restrict__ z    = (double complex*) malloc(sizeof(double complex)*N);
	//
	//@@srand(time(NULL));
	//
	int ii;
	for (ii = 0; ii < N; ++ii)
	{
		x[ii] = 30*((double)rand() / (double)RAND_MAX - .5) + 30*((double)rand() / (double)RAND_MAX - .5)*I;
	}	
	//
	long c0, c1;
	//
	c0 = rdtscp();
	marla_cexp_vec( y, x, N );
	//marla_exp_vec_svml( y, x, N );
	c1 = rdtscp();
	long long cm = (c1 - c0)/N;
	printf("marla exp : %ld cycles\n", cm); fflush(stdout);
	//
	c0 = rdtscp();
	for (ii = 0; ii < N; ++ii)
	{
		z[ii] = cexp(x[ii]);
	}
	c1 = rdtscp();
	printf("lib    exp : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	//
#ifdef __WITH_MPRF
	mpfr_check(N, x, y, &mpfr_cexp);
	//
#else
	for (int ii = 0; ii < 10; ++ii)
	{
		printf("%d: x = %.15f + I%.15f, exp = %.15f + I%.15f  (%.15f + I%.15f), error = ", ii, creal(x[ii]), cimag(x[ii]), creal(y[ii]), cimag(y[ii]), z[ii]);
		printf("%.15f", fabs((y[ii] - z[ii])));
				printf("\n");
				}	
#endif
	//
	free(x);
	free(y);
	free(z);
	//
}
