#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <immintrin.h>
//
#include "timers.h"
#include "utils.h"
#include "sin.h"
#include "floor.h"
#include "ceil.h"
#include "exp_lut.h"
//
#define NA 10
#define NI 10
//
// forward declarations
//

double marla_floor_overflow(double* );

#if 0
inline
double pass1(const double x)
{
        return x + largeint1;
}

inline
double pass2(const double tx)
{
	const uint64_t kTMP = (uint64_t) largeint; 
	uint64_t lx = *((uint64_t*) &tx) - kTMP;
	return *((double*) &kTMP);
}
#endif
//
//
//
int main ()
{
  const char * format = "%.1f \t%.1f \t%.1f \t%.1f \t%.1f \t%.1f \t%.1f \t%.1f \t%.1f \t%.1f \t%.1f\n";
  printf ("value\tround\t+0.5\t-0.5\tfloor\t+0.5\tceil\ttrunc\t+- 0.5\trint\t+0.5\n");
  printf ("-----\t-----\t-----\t-----\t----\t-----\t-----\r-----\n");
  printf (format, 2.3, round( 2.3), round(2.3 + 0.5), round(2.3 - 0.5), floor( 2.3),  floor( 2.3 + 0.5), ceil( 2.3),trunc( 2.3), trunc(2.3 + 0.5), rint(2.3), rint(2.3 + 0.5));
  printf (format, 3.8, round( 3.8), round(3.8 + 0.5), round(3.8 - 0.5), floor( 3.8),  floor( 3.8 + 0.5), ceil( 3.8),trunc( 3.8), trunc(3.8 + 0.5), rint(3.8), rint(3.8 + 0.5));
  printf (format, 5.5, round( 5.5), round(5.5 + 0.5), round(5.5 - 0.5), floor( 5.5),  floor( 5.5 + 0.5), ceil( 5.5),trunc( 5.5), trunc(5.5 + 0.5), rint(5.5), rint(5.5 + 0.5));
  printf (format,-2.3, round(-2.3), round(-2.3 + 0.5), round(-2.3 - 0.5), floor(-2.3),  floor(-2.3 + 0.5), ceil(-2.3),trunc(-2.3), trunc(-2.3 - 0.5), rint(-2.3), rint(-2.3 + 0.5));
  printf (format,-3.8, round(-3.8), round(-3.8 + 0.5), round(-3.8 - 0.5), floor(-3.8),  floor(-3.8 + 0.5), ceil(-3.8),trunc(-3.8), trunc(-3.8 - 0.5), rint(-3.8), rint(-3.8 + 0.5));
  printf (format,-5.5, round(-5.5), round(-5.5 + 0.5),  round(-5.5 - 0.5), floor(-5.5),  floor(-5.5 + 0.5), ceil(-5.5),trunc(-5.5), trunc(-5.5 - 0.5), rint(-5.5), rint(-5.5 + 0.5));
  return 0;
}

