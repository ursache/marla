#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
//
#include "timers.h"
#include "rcp.h"
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
void marla_rcp_vec(double*       __restrict__ y,
		   const double* __restrict__ x,
		   const int                  n)
{
    for(int i = 0; i < n; ++i)
        y[i] = marla_rcp(x[i]);
}
//
void marla_rcp_nr1_vec(double*   __restrict__ y,
                   const double* __restrict__ x,
                   const int                  n)
{
    for(int i = 0; i < n; ++i)
        y[i] = marla_rcp_nr1(x[i]);
}
//
void marla_rcp_nr2_vec(double*   __restrict__ y,
                   const double* __restrict__ x,
                   const int                  n)
{
    for(int i = 0; i < n; ++i)
        y[i] = marla_rcp_nr2(x[i]);
}
//
int  main()
{
        const int N     = NA;
        const int NITER = NI;
        //
        double* __restrict__ x   = (double*) malloc(sizeof(double)*N);
        double* __restrict__ y   = (double*) malloc(sizeof(double)*N);
        double* __restrict__ y1  = (double*) malloc(sizeof(double)*N);
        double* __restrict__ y2  = (double*) malloc(sizeof(double)*N);
        double* __restrict__ y3  = (double*) malloc(sizeof(double)*N);
        double* __restrict__ y4  = (double*) malloc(sizeof(double)*N);
        //
        srand(time(NULL));
        //
        for (int ii = 0; ii < N; ++ii)
        {
                x[ii] = 10.*((double)rand() / (double)RAND_MAX);
        }
        //
        long c0, c1;
        //
        // benchmarking
        //
        c0 = rdtscp();
        marla_rcp_vec( y, x, N );
        c1 = rdtscp();
        printf("rcp     %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
        marla_rcp_nr1_vec( y1, x, N );
        c1 = rdtscp();
        printf("rcp RN1 %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
        marla_rcp_nr2_vec( y2, x, N );
        c1 = rdtscp();
        printf("rcp RN2 %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
#if 1
        for (int ii = 0; ii < 10; ++ii)
        {
                //if (y[ii] != exp(x[ii]))
		printf("%d: 1./%.15f = %.15f NR1: %.15f NR2: %.15f, error = %.15f NR1: %.15f NR2: %.15f\n", ii, x[ii], y[ii], y1[ii], y2[ii], fabs(y[ii] - 1./x[ii]), 
				fabs(y1[ii] - 1./x[ii]), 
				fabs(y2[ii] - 1./x[ii])
		      );
        }
#endif
        //
        free(x);
        free(y);
        //
}

