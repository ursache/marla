#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#ifdef __WITH_MPFR
#include <gmp.h>
#include <mpfr.h>
//#include "mpfr_check.h"
#endif
//
#include <stdint.h>
#include "timers.h"
#include "exp.h"
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
#pragma omp declare simd uniform(x) aligned(x:32) uniform(y) aligned(y:32)
void marla_exp_vec( double* __restrict__ y, const double* __restrict__ x, int N )
{
        for (int ii = 0; ii < N; ++ii)
                y[ii] = marla_exp(x[ii]);
}
//
int main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	double* __restrict__ x    = (double*) malloc(sizeof(double)*N);
	double* __restrict__ y    = (double*) malloc(sizeof(double)*N);
	double* __restrict__ z    = (double*) malloc(sizeof(double)*N);
	//
	//@@srand(time(NULL));
	//
	int ii;
	for (ii = 0; ii < N; ++ii)
	{
		x[ii] = 30*((double)rand() / (double)RAND_MAX - .5);
	}	
	//
	long c0, c1;
	//
	c0 = rdtscp();
	marla_exp_vec( y, x, N );
	//marla_exp_vec_svml( y, x, N );
	c1 = rdtscp();
	long long cm = (c1 - c0)/N;
	printf("marla exp : %ld cycles\n", cm); fflush(stdout);
	//
	c0 = rdtscp();
	for (ii = 0; ii < N; ++ii)
	{
		z[ii] = exp(x[ii]);
	}
	c1 = rdtscp();
	printf("lib    exp : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	//
#ifdef __WITH_MPRF
	mpfr_check(N, x, y, &mpfr_exp);
	//
#else
	for (int ii = 0; ii < 10; ++ii)
	{
		printf("%d: x = %.15f, exp = %.15f (%.15f), error = ", ii, x[ii], y[ii], z[ii]);
		printf("%.15f", fabs((y[ii] - z[ii])));
				printf("\n");
				}	
#endif
	//
	free(x);
	free(y);
	free(z);
	//
}
