#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <float.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#ifdef __WITH_MPFR
#include <gmp.h>
#include <mpfr.h>
#include "mpfr_check.h"
#endif
//
#include "timers.h"
#include "utils.h"
#include "powf.h"
//
#define NA 10000000
#define NI 10
//
#ifdef __riscv
#warning "riscv"
#endif
//
// forward declarations
//
void marla_powf_vec( float* __restrict__ z, const float* __restrict__ y, const float* __restrict__ x, int N )
{
        for (int ii = 0; ii < N; ++ii)
                z[ii] = marla_powf(x[ii], y[ii]);
}
//
//
//
int main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	float* __restrict__ x    = (float*) malloc(sizeof(float)*N);
	float* __restrict__ y    = (float*) malloc(sizeof(float)*N);
	float* __restrict__ z    = (float*) malloc(sizeof(float)*N);
	float* __restrict__ zz    = (float*) malloc(sizeof(float)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = fabs((float) 10.*((float)rand() / (float)RAND_MAX - .5));
		y[ii] = ((float)rand() / (float)RAND_MAX - .5);
	}	
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_powf_vec( z, y, x, N );
	//sleep(10);
        c1 = rdtscp();
	long long cm = (c1 - c0)/N;
	printf("marla powf : %ld cycles\n", cm); fflush(stdout);
        //
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
	{
		zz[ii] = powf(x[ii], y[ii]);
	}
        c1 = rdtscp();
        printf("lib   powf : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	//
#ifdef __WITH_MPFR
	mpfr_check2(N, y, x, z, &mpfr_powf);
#else
	for (int ii = 0; ii < 10; ++ii)
	{
		printf("%d: x = %.15f, y = %.15f, powf = %.15f (%.15f), error = ", ii, x[ii], y[ii], z[ii], zz[ii]);
		//#ifdef __WITH_MPFR
		//			mpfr_out_str (stdout, 10, 0, ymp[ii], MPFR_RNDN);
		//#else
		printf("%.15f", (zz[ii] - z[ii]));
		//#endif
		printf("\n");	

	}
#endif
	//
	free(x);
	free(y);
	free(z);
	free(zz);
	//
}

