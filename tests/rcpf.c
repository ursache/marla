#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
//
#include "timers.h"
#include "rcpf.h"
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
void marla_rcpf_vec(float*       __restrict__ y,
		   const float* __restrict__ x,
		   const int                  n)
{
    for(int i = 0; i < n; ++i)
        y[i] = marla_rcpf(&x[i]);
}
//
void marla_rcpf_nr1_vec(float*   __restrict__ y,
                   const float* __restrict__ x,
                   const int                  n)
{
    for(int i = 0; i < n; ++i)
        y[i] = marla_rcpf_nr1(&x[i]);
}
//
void marla_rcpf_nr2_vec(float*   __restrict__ y,
                   const float* __restrict__ x,
                   const int                  n)
{
    for(int i = 0; i < n; ++i)
        y[i] = marla_rcpf_nr2(&x[i]);
}
//
int  main()
{
        const int N     = NA;
        const int NITER = NI;
        //
        float* __restrict__ x   = (float*) malloc(sizeof(float)*N);
        float* __restrict__ y   = (float*) malloc(sizeof(float)*N);
        float* __restrict__ y1  = (float*) malloc(sizeof(float)*N);
        float* __restrict__ y2  = (float*) malloc(sizeof(float)*N);
        float* __restrict__ y3  = (float*) malloc(sizeof(float)*N);
        float* __restrict__ y4  = (float*) malloc(sizeof(float)*N);
        //
        srand(time(NULL));
        //
        for (int ii = 0; ii < N; ++ii)
        {
                x[ii] = 10.*((float)rand() / (float)RAND_MAX);
        }
        //
        long c0, c1;
        //
        // benchmarking
        //
        c0 = rdtscp();
        marla_rcpf_vec( y, x, N );
        c1 = rdtscp();
        printf("rcpf     %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
        marla_rcpf_nr1_vec( y1, x, N );
        c1 = rdtscp();
        printf("rcpf RN1 %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
        marla_rcpf_nr2_vec( y2, x, N );
        c1 = rdtscp();
        printf("rcpf RN2 %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
#if 1
        for (int ii = 0; ii < 10; ++ii)
        {
                //if (y[ii] != exp(x[ii]))
		printf("%d: 1./%.15f = %.15f NR1: %.15f NR2: %.15f, error = %.15f NR1: %.15f NR2: %.15f\n", ii, x[ii], y[ii], y1[ii], y2[ii], fabs(y[ii] - 1./x[ii]), 
				fabs(y1[ii] - 1./x[ii]), 
				fabs(y2[ii] - 1./x[ii])
		      );
        }
#endif
        //
        free(x);
        free(y);
        //
}

