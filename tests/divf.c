#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
//
#include "timers.h"
#include "divf.h"
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
void marla_divf_vec(float*       __restrict__ z,
		   const float* __restrict__ x,
		   const float* __restrict__ y,
		   const int                  n)
{
    for(int i = 0; i < n; ++i)
        z[i] = marla_divf(x[i], y[i]);
}
//
void marla_divf_nr1_vec(float*   __restrict__ z,
                   const float* __restrict__ x,
		   const float* __restrict__ y,
                   const int                  n)
{
    for(int i = 0; i < n; ++i)
        z[i] = marla_divf_nr1(&x[i], &y[i]);
}
//
void marla_divf_nr2_vec(float*   __restrict__ z,
                   const float* __restrict__ x,
		   const float* __restrict__ y,
                   const int                  n)
{
    for(int i = 0; i < n; ++i)
        z[i] = marla_divf_nr2(&x[i], &y[i]);
}
//
void marla_divf_nr3_vec(float*   __restrict__ z,
                   const float* __restrict__ x,
		   const float* __restrict__ y,
                   const int                  n)
{
    for(int i = 0; i < n; ++i)
        z[i] = marla_divf_nr3(&x[i], &y[i]);
}
//
void marla_divf_nr4_vec(float*   __restrict__ z,
                   const float* __restrict__ x,
		   const float* __restrict__ y,
                   const int                  n)
{
    for(int i = 0; i < n; ++i)
        z[i] = marla_divf_nr4(&x[i], &y[i]);
}
//
int  main()
{
        const int N     = NA;
        const int NITER = NI;
        //
        float* __restrict__ x   = (float*) malloc(sizeof(float)*N);
        float* __restrict__ y   = (float*) malloc(sizeof(float)*N);
        float* __restrict__ z   = (float*) malloc(sizeof(float)*N);
        float* __restrict__ z1  = (float*) malloc(sizeof(float)*N);
        float* __restrict__ z2  = (float*) malloc(sizeof(float)*N);
        //
        srand(time(NULL));
        //
        for (int ii = 0; ii < N; ++ii)
        {
                x[ii] = 10.*((float)rand() / (float)RAND_MAX);
                y[ii] = 10.*((float)rand() / (float)RAND_MAX);
        }
        //
        long c0, c1;
        //
        // benchmarking
        //
        c0 = rdtscp();
        marla_divf_vec( z, x, y, N );
        c1 = rdtscp();
        printf("divf     %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
        marla_divf_nr1_vec( z1, x, y, N );
        c1 = rdtscp();
        printf("divf RN1 %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        c0 = rdtscp();
        marla_divf_nr2_vec( z2, x, y, N );
        c1 = rdtscp();
        printf("divf RN2 %ld cycles\n", (c1 - c0)/N); fflush(stdout);
        //
        //
#if 1
        for (int ii = 0; ii < 10; ++ii)
        {
                //if (y[ii] != exp(x[ii]))
		printf("%d: %.15f/%.15f = %.15f %.15f, error = %.15f %.15f\n", ii, x[ii], y[ii], z1[ii], z2[ii], fabs(z2[ii] - x[ii]/y[ii]), 
				fabs(z1[ii] - x[ii]/y[ii]),  
				fabs(z2[ii] - x[ii]/y[ii])
		      );
        }
#endif
        //
        free(x);
        free(y);
        free(z);
        free(z1);
        free(z2);
        //
}

