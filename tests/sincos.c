#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <float.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#ifdef __WITH_MPFR
#include <gmp.h>
#include <mpfr.h>
#include "mpfr_check.h"
#endif
//
#include "timers.h"
#include "utils.h"
#include "sincos.h"
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
void marla_sincos_vec( double* __restrict__ x, double* __restrict__ s,  double* __restrict__ c, int N )
{
        for (int ii = 0; ii < N; ++ii)
                marla_sincos(x[ii], &s[ii], &c[ii]);
}
 
//
int main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	double* __restrict__ x    = (double*) malloc(sizeof(double)*N);
	double* __restrict__ z    = (double*) malloc(sizeof(double)*N);
	double* __restrict__ s    = (double*) malloc(sizeof(double)*N);
	double* __restrict__ sn   = (double*) malloc(sizeof(double)*N);
	double* __restrict__ c    = (double*) malloc(sizeof(double)*N);
	double* __restrict__ cn   = (double*) malloc(sizeof(double)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = 3.14159*((double)rand() / (double)RAND_MAX - .5);
	}	
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_sincos_vec( x, s, c, N );
        c1 = rdtscp();
	long long cm = (c1 - c0)/N;
	printf("marla sincos : %ld cycles\n", cm); fflush(stdout);
        //
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
	{
		sincos(x[ii], &sn[ii], &cn[ii]);
	}
        c1 = rdtscp();
        printf("lib   sincos : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
	//
	//
#ifdef __WITH_MPFR
	//mpfr_check(N, x, y, &mpfr_sincos);
#else
	for (int ii = 0; ii < 10; ++ii)
        {
			printf("%d: x = %.15f, sin = %.15f (%.15f), cos = %.15f (%.15f), error = ", ii, x[ii], s[ii], sn[ii], c[ii], cn[ii]);
			printf("%.15f, %.15f", fabs(s[ii] - sn[ii]), fabs(c[ii] - cn[ii]));
			printf("\n");	
	 
	}
#endif
	//
	free(x);
	free(z);
	free(c);
	free(s);
	free(cn);
	free(sn);
	//
}

