#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <float.h>
#include <stdint.h>
#include <time.h>
#ifdef __WITH_MPFR
#include <gmp.h>
#include <mpfr.h>
#include "mpfr_check.h"
#endif
//
#include "timers.h"
#include "utils.h"
#include "cosf.h"
//
#define NA 10000000
#define NI 10
//
// forward declarations
//
void marla_cosf_vec( float* __restrict__ y, const float* __restrict__ x, int N )
{
        for (int ii = 0; ii < N; ++ii)
                y[ii] = marla_cosf(x[ii]);
}
 
//
//
//
int main()
{
	const int N     = NA;
	const int NITER = NI;
	//
	float* __restrict__ x    = (float*) malloc(sizeof(float)*N);
	float* __restrict__ y    = (float*) malloc(sizeof(float)*N);
	float* __restrict__ z    = (float*) malloc(sizeof(float)*N);
	//
	srand(time(NULL));
	//
	for (int ii = 0; ii < N; ++ii)
	{
		x[ii] = 100.*((float)rand() / (float)RAND_MAX - .5);
	}	
	x[0] = -3.1416/2.;
	//
	long c0, c1;
	//
	// benchmarking
	//
        c0 = rdtscp();
	marla_cosf_vec( y, x, N );
        c1 = rdtscp();
	long long cm = (c1 - c0)/N;
	printf("marla cos : %ld cycles\n", cm); fflush(stdout);
        //
        c0 = rdtscp();
	for (int ii = 0; ii < N; ++ii)
	{
		z[ii] = cosf(x[ii]);
	}
        c1 = rdtscp();
        printf("lib   cos : %ld cycles\n", (c1 - c0)/N); fflush(stdout);
#ifdef __WITH_MPFR
	mpfr_checkf(N, x, y, mpfr_cos);
	//
#else
	for (int ii = 0; ii < 10; ++ii)
        {
			printf("%d: x = %.15f, cos = %.15f (%.15f), error = ", ii, x[ii], y[ii], z[ii], fabs(y[ii] - z[ii]));
			printf("%.15f", fabs(y[ii] - z[ii]));
			printf("\n");	
	 
	}
#endif
	//
	free(x);
	free(y);
	//
}

