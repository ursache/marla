#pragma once

#include "sinf.h"
//
//#pragma option_override(marla_cosf,  "opt(level, 2)")
inline
float marla_cosf(float x)
{
	return marla_sinf(x + (float) PIO2f);
}

