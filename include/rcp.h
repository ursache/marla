#pragma once

#define RCP \
	float fa = (float) a; double r   = 1.f/fa;
#define RCP_NR r = r*(2. - a*r);

inline
double marla_rcp(double a)
{
	float fa = (float) a; double r   = 1.f/fa;
	return r;
}

inline
double marla_rcp_nr1(double a)
{
	float fa = (float) a; double r   = 1.f/fa;
	r = r*(2. - a*r);
        return r;
}

inline
double marla_rcp_nr2(double a)
{
	float fa = (float) a; double r   = 1.f/fa;
	r = r*(2. - a*r);
	r = r*(2. - a*r);
        return r;
}

inline
double marla_rcp_nr3(double a)
{
	float fa = (float) a; double r   = 1.f/fa;
	r = r*(2. - a*r);
	r = r*(2. - a*r);
	r = r*(2. - a*r);
        return r;
}

inline
double marla_rcp_nr4(double a)
{
	float fa = (float) a; double r   = 1.f/fa;
	r = r*(2. - a*r);
	r = r*(2. - a*r);
	r = r*(2. - a*r);
	r = r*(2. - a*r);
        return r;
}
