#pragma once

#include <stdio.h>

#include "constf.h"
#include "polevlf.h"
#include "floorf.h"


/*	Exponential function	*/

float PPf[] = {
	1.26177193074810590878E-4f,
	3.02994407707441961300E-2f,
	9.99999999999999999910E-1f,
};
float QQf[] = {
	3.00198505138664455042E-6f,
	2.52448340349684104192E-3f,
	2.27265548208155028766E-1f,
	2.00000000000000000009E0f,
};

float C1f = 6.93145751953125E-1f;
float C2f = 1.42860682030941723212E-6f;

extern float marla_floorf ( const float );
extern float marla_ldexpf ( float, int );
extern float LOGE2f, LOG2Ef, MAXLOGf, MINLOGf, MAXNUMf;


inline static 
float marla_expf(float x)
{
	float px, xx;
	int n;

	/* Express e**x = e**g 2**n
	 *   = e**g e**( n loge(2) )
	 *   = e**( g + n loge(2) )
	 */
	float arg = LOG2Ef*x + 0.5f;	
	//px = marla_floorf (arg);	/* floor() truncates toward -infinity. */
	px = floorf (arg);	/* floor() truncates toward -infinity. */
	//printf("--> %f %f\n", LOG2Ef * x + 0.5, px); fflush(stdout);
	n = px;
	//
	x -= px*C1f;
	x -= px*C2f;
	/* 
	 * rational approximation for exponential
	 * of the fractional part:
	 * e**x = 1 + 2x PP(x**2)/( QQ(x**2) - PP(x**2) )
	 */
	xx = x*x;
	px = x*marla_polevlf (xx, PPf, 2);
	x  = px/(marla_polevlf(xx, QQf, 3) - px);
	//x  = marla_divf(px, (marla_polevlf (xx, QQ, 3) - px));
	x  = 1.0f + 2.0f*x;

	/* multiply by power of 2 */
	//x = ldexp (x, abs(n));
	//if (n < 0) p2 = marla_divf(1.f, p2);
	float p2 = (float) (1 << abs(n));
	if (n < 0) p2 = 1.f/p2;
	x = x*p2;
	//
	return (x);
}

