#pragma once
#include <stdio.h>
//
//extern float MAXNUM, NEGZERO;
//
inline
float marla_ldexpf( float x, int pw2 )
{
	//
	uint32_t z = *((uint32_t *) &x);
	uint16_t e; 
	//	
	e = (z >> 23) & 0xff; 
        e += pw2;
	//
	z = (z & 0x807fffff) | e << 23;
	return *(float*) &z;
}
