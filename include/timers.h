#include <stdio.h>
#include <stdlib.h>
#ifdef __amd64__
static
__inline__ uint64_t rdtsc(void) {
        uint32_t l, h;
        __asm__ __volatile__ ("xorl %%eax,%%eax\ncpuid"
                              ::: "%rax", "%rbx", "%rcx", "%rdx");
        __asm__ __volatile__ ("rdtsc" : "=a" (l), "=d" (h));
        return (uint64_t)h << 32 | l;
}

static
__inline__
unsigned long long rdtscp()
{
        unsigned int cycles_high, cycles_low;
        __asm__ volatile (
                        "RDTSCP\n\t"/*read the clock*/
                        "mov %%edx, %0\n\t"
                        "mov %%eax, %1\n\t"
                        "CPUID\n\t": "=r" (cycles_high), "=r"
                        (cycles_low):: "%rax", "%rbx", "%rcx", "%rdx");
        return ((unsigned long long)cycles_low + ((unsigned long long)cycles_high << 32));
}
#elif defined(__riscv)
static 
__inline__
uint64_t 
__attribute__((__gnu_inline__, __always_inline__, __artificial__))
rdtscp(void)
{
    // from https://stackoverflow.com/questions/52187221/how-to-calculate-the-no-of-clock-cycles-in-riscv-clang
    unsigned long dst;
    // output into any register, likely a0
    // regular instruction:
    asm volatile ("csrrs %0, 0xc00, x0" : "=r" (dst) );
    // regular instruction with symbolic csr and register names
    // asm volatile ("csrrs %0, cycle, zero" : "=r" (dst) );
    // pseudo-instruction:
    // asm volatile ("csrr %0, cycle" : "=r" (dst) );
    // pseudo-instruction:
    //asm volatile ("rdcycle %0" : "=r" (dst) );
    return dst;
}

//static
//__inline__ uint64_t rdtscp()
//{
//	return 1.;
//}
#endif

