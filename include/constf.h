#pragma once
#ifndef MARLA_CONSTF
#define MARLA_CONSTF

float MACHEPf   =  1.11022302462515654042E-16;   /* 2**-53 */
float UFLOWTHRESHf =  2.22507385850720138309E-308; /* 2**-1022 */
float MAXLOGf   =  7.08396418532264106224E2;     /* log 2**1022 */
float MINLOGf   = -7.08396418532264106224E2;     /* log 2**-1022 */
float MAXNUMf   =  1.79769313486231570815E308;    /* 2**1024*(1-MACHEP) */
float PIf       =  3.14159265358979323846;       /* pi */
float INVPIf    =  0.31830988618379067153;       /* 1./pi */
float PIO2f     =  1.57079632679489661923;       /* pi/2 */
float PIO4f     =  7.85398163397448309616E-1;    /* pi/4 */
float SQRT2f    =  1.41421356237309504880;       /* sqrt(2) */
float SQRTHf    =  7.07106781186547524401E-1;    /* sqrt(2)/2 */
float LOG2Ef    =  1.4426950408889634073599;     /* 1/log(2) */
float SQ2OPIf   =  7.9788456080286535587989E-1;  /* sqrt( 2/pi ) */
float LOGE2f    =  6.93147180559945309417E-1;    /* log(2) */
float LOGSQ2f   =  3.46573590279972654709E-1;    /* log(2)/2 */
float THPIO4f   =  2.35619449019234492885;       /* 3*pi/4 */
float TWOOPIf   =  6.36619772367581343075535E-1; /* 2/pi */
//float INFINITY =  1.79769313486231570815E308;    /* 2**1024*(1-MACHEP) */
//float NAN      = 0.0;
float NEGZEROf  = 0.0;
//
int     	 __fShift   = 0x4b400000;
unsigned int     __fAbsMask = 0x7fffffff;
#endif
