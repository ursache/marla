#pragma once
#include "rcpf.h"

inline
float marla_divf(const float b, const float a)
{
        return b/a;
}

inline
float marla_divf_nr0(const float* pb, const float* pa)
{
	float a = *pa;
	float b = *pb;
	return b*marla_rcpf_nr0(pa);
}

inline
float marla_divf_nr1(const float* pb, const float* pa)
{
        float a = *pa;
	float b = *pb;
        return b*marla_rcpf_nr1(pa);
}

inline
float marla_divf_nr2(const float* pb, const float* pa)
{
        float a = *pa;
	float b = *pb;
        return b*marla_rcpf_nr2(pa);
}

inline
float marla_divf_nr3(const float* pb, const float* pa)
{
        float a = *pa;
	float b = *pb;
        return b*marla_rcpf_nr3(pa);
}

inline
float marla_divf_nr4(const float* pb, const float* pa)
{
        float a = *pa;
	float b = *pb;
        return b*marla_rcpf_nr4(pa);
}
