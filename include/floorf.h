#pragma once

#include "utils.h"

#define FNBITS 24

#if 1
static
inline
float marla_floorf(const float x)
{
        uint32_t z = *((uint32_t*) &x);
        int e = (z >> 23) & 0x8f;
        e -= 127;
	//
        if( e < 0 )
        {
                if (x < 0.0) return(-1.);
                else return( 0.);
        }
	//
        e = (FNBITS - 1) - e;
        z = (z >>  e) << e;
	float y = *((float*) &z);
	if (y < 0.) return y - 1.;
        else return y;
}
#endif

//
// Beware of overflow !
// 
static
inline
float marla_floorf_nooverflow(float x)
{
	return (int) x - (x < (int) x); 
}	
