#pragma once

#include "frexp.h"
#include "ldexp.h"

#define quake3

inline
double marla_sqrt(double x)
{
        int e;
        short *q;
        double z, w;
	//
        if( x == 0.0 ) return( 0.0 );
        w = x;
#if defined(marla)
        /* Note, frexp and ldexp are used in order to
         * handle denormal numbers properly.
         */
        z = marla_frexp( &x, &e );
        /* approximate square root of number between 0.5 and 1
         * relative error of approximation = 7.47e-3
         */
        x = 4.173075996388649989089e-1 + 5.9016206709064458299663e-1*z;
        /* adjust for odd powers of 2 */
        if( (e & 1) != 0 ) x *= SQRT2;
        /* re-insert exponent */
        e = e >> 1;
        x = marla_ldexp( x, e );
        /* 
	 * Note, assume the square root cannot be denormal,
         * so it is safe to use integer exponent operations here.
         */
#elif defined(quake3)
        int    i;
        float  y;
	double x2;
        const double threehalfs = 1.5;

        x2 = x*0.5;
        y  = x;
        i  = * ( int * ) &y;                       // evil floating point bit level hacking
        i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
        y  = * ( float * ) &i;
	z = (double) y;
        z  = z * ( threehalfs - ( x2 * z * z ) );   // 1st iteration
        z  = z * ( threehalfs - ( x2 * z * z ) );   // 2nd iteration, this can be removed
        z  = z * ( threehalfs - ( x2 * z * z ) );   // 2nd iteration, this can be removed
        z  = z * ( threehalfs - ( x2 * z * z ) );   // 2nd iteration, this can be removed
        return x*z;	
#else
	x = x*1.f/sqrtf(x);
#endif
        x = 0.5*(x + w/x); 
        x = 0.5*(x + w/x); 
        //x = 0.5*(x + w/x);  
        return(x);
}
