#pragma once

//#include "utils.h"
#include <stdint.h>

inline static
float marla_frexpf( float x, int* pw2)
{
        uint32_t z = *((uint32_t *) &x);
        const uint32_t pow = ((z >> 23) & 0xff) - 126;
        z = (z & 0x807fffff) | 0x3f000000;
        float man = *(float *) &z;
        *pw2  = pow;

        return man;
}

#if 0
	void
myfrexp(float* __restrict__ z, int* __restrict__ y, double* __restrict__ x, int N)
{
	for (int ii = 0; ii < N; ++ii)
	{
		z[ii] = frexp_vec(&y[ii], &x[ii]);
	}
}
#endif
