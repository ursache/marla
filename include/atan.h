#pragma once

#include "const.h"
#include "polevl.h"

double T3P8 = 2.41421356237309504880;
double SNAN  = 0x7F800001;
//static double NAN   = 0xFFFFFFFF;


#define MOREBITS 6.123233995736765886130E-18
#define MAXNUM 1.7976931348623158E308

double PA[5] = {
-8.750608600031904122785E-1,
-1.615753718733365076637E1,
-7.500855792314704667340E1,
-1.228866684490136173410E2,
-6.485021904942025371773E1,
};
//
double QA[5] = {
/* 1.000000000000000000000E0, */
 2.485846490142306297962E1,
 1.650270098316988542046E2,
 4.328810604912902668951E2,
 4.853903996359136964868E2,
 1.945506571482613964425E2,
};
//
// atan
//
inline
double marla_atan(const double px)
{
	/* make argument positive and save the sign */
	int sign = 1;
	double y, z;
	double x = px;
	if( x < 0.0 )
	{
		sign = -1;
		x = -x;
	}
	/* range reduction */
	int flag = 0;
	if( x > T3P8 )
	{
		y = PIO2;
		flag = 1;
		x = -1.0/x;
	}
	else if( x <= 0.66 ) y = 0.0;
	else
	{
		y = PIO4;
		flag = 2;
		x = (x - 1.0)/(x + 1.0);
	}
	z = x * x;
	z = z * marla_polevl( z, PA, 4 ) / marla_p1evl( z, QA, 5 );
	z = x * z + x;
	if( flag == 2 ) z += 0.5 * MOREBITS;
	else if( flag == 1 ) z += MOREBITS;
	y = y + z;
	if( sign < 0 ) y = -y;
	return y;
}
//
// atan2
//
inline
double marla_atan2( const double y, const double x )
{
	double z;
	if ((x == 0) && (y == 0)) z = SNAN; 
	else
	{
		double ratio = y/x;
		z = marla_atan(ratio);
		if ((x < 0.) && (y < 0.)) z -= PI;
		if ((x < 0.) && (y > 0.)) z += PI;
	}
	return z;
}
