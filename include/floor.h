#pragma once
#include <stdint.h>

#define NBITS 53

inline
double marla_floor(const double x)
{
        uint64_t z = *((uint64_t*) &x);
        int e = (z >> 52) & 0x7ff;
        e -= 1023;
	//
        if( e < 0 )
        {
                if (x < 0.) return(-1.);
                else return( 0.);
        }
	//
		e = (NBITS - 1) - e;
		z = (z >>  e) << e;
		double y = *(double*) &z;
		if (y < 0.)
			return y - 1.;
		else 
			return y;
}
//
// Beware of overflow !
// 
inline
double marla_floor_overflow(double x)
{
	return (int) x - (x < (int) x); 
}
