#pragma once

//#define exp marla_exp_lut

#include <stdio.h>
#include "const.h"
#include "ldexp.h"
#include "polevl.h"
#include "floor.h"
#include "div.h"
//
#include "exp_lut.h"
//
//#define marla_exp marla_exp_cephes


/*	Exponential function	*/

static double PP[] = {
	1.26177193074810590878E-4,
	3.02994407707441961300E-2,
	9.99999999999999999910E-1,
};
static double QQ[] = {
	3.00198505138664455042E-6,
	2.52448340349684104192E-3,
	2.27265548208155028766E-1,
	2.00000000000000000009E0,
};

static double C1 = 6.93145751953125E-1;
static double C2 = 1.42860682030941723212E-6;

extern double floor ( double );
extern double ldexp ( double, int );
extern double LOGE2, LOG2E, MAXLOG, MINLOG, MAXNUM;
/*
inline static
double myfloor(const double x)
{
	return floor(x);
}
*/
inline static
double marla_exp(double x)
{
	//double px, xx, x = *in;
	if (x > MAXLOG) return MAXNUM;
	if (x < MINLOG) return 0.0;
	double px, xx;
	int n;

	/* Express e**x = e**g 2**n
	 *   = e**g e**( n loge(2) )
	 *   = e**( g + n loge(2) )
	 */
	px = floor (LOG2E * x + 0.5);	/* floor() truncates toward -infinity. */
	n = px;
	//
	x -= px * C1;
	x -= px * C2;
	/* 
	 * rational approximation for exponential
	 * of the fractional part:
	 * e**x = 1 + 2x PP(x**2)/( QQ(x**2) - PP(x**2) )
	 */
	xx = x*x;
	px = x*marla_polevl (xx, PP, 2);
	x  = px/(marla_polevl (xx, QQ, 3) - px);
	//x  = marla_div(px, (marla_polevl (xx, QQ, 3) - px));
	x  = 1.0 + 2.0*x;

	/* multiply by power of 2 */
	//x = ldexp (x, n);
	double p2 = (double) (1 << abs(n));
	//if (n < 0) p2 = marla_div(1., p2);
	if (n < 0) p2 = 1./p2;
	x = x*p2;
	//
	return (x);
}

inline static
double fast_pow2(double d) 
{
	float x = d;
	// store address of float as pointer to long
	int *px = (int*)(&x);
	// temporary value for truncation: x-0.5 is added to a large integer (3<<22)
	float tx = (x-0.5f) + (3<<22);
	int lx = *((int*)&tx) - 0x4b400000;   // integer value of x
	float dx = x-(float)(lx);             // float remainder of x
	x = 1.0f + dx*(0.693153f              // polynomial apporoximation of 2^x
			+ dx*(0.240153f              // for x in the range [0, 1]
				+ dx*(0.0558282f
					+ dx*(0.00898898f
						+ dx* 0.00187682f ))));
	*px += (lx<<23);                      // add integer power of 2 to exponent
	return x;
}


long long lIndexMask = 0x00000000000003ff;
long long dbShifter  = 0x4338000000000000;


#if 1
inline
double marla_exp_lut(double x)
{
        if (x > MAXLOG) return MAXNUM;
        if (x < MINLOG) return 0.0;
	//
	double N = round(x*1024./log(2)); // round is rint
        double M = ((int) N/1024);         // integer division
        double r = x - N*log(2)/1024.;    //
	//
        double P    = 1. + r*(1. + r*(0.5 + r*(1./6. + 1.*r/24.)));
	//
	double dM = x*1024./log(2) + *((double*) &dbShifter);
	//
	long long lIndex = (*(long long*) &dM) & lIndexMask;
	N = dM - *((double*) &dbShifter);
	//
	long long lM = (*(long long*)&dM)&(~lIndexMask);
	lM = lM << 42;
	double val = *((double*) &exp_lut[lIndex])*P;
	long long sum = *((long long*) &val) + lM;
	val = *((double*) &sum);
	return val;
}
#endif


#if 0
//inline
void marla_exp_svml(double* __restrict__ y, const double* __restrict__ x)
{
	long long dbInvLn2   = 0x40971547652b82fe; __m256d __dbInvLn2  = _mm256_set1_pd(*((double*) &dbInvLn2)); 
	long long dbShifter  = 0x4338000000000000; __m256d __dbShifter = _mm256_set1_pd(*((double*) &dbShifter)); 
	long long lIndexMask = 0x00000000000003ff; __m256d __lIndexMask = _mm256_set1_pd(*((double*) &lIndexMask)); 
	//
	long long dPC0       = 0x3ff0000000000000; __m256d __dPC0 = _mm256_set1_pd(*((double*) &dPC0)); 
	long long dPC1       = 0x3fe0000001ebfbe0; __m256d __dPC1 = _mm256_set1_pd(*((double*) &dPC1)); 
	long long dPC2       = 0x3fc5555555555556; __m256d __dPC2 = _mm256_set1_pd(*((double*) &dPC2)); 
	//
	long long dbLn2hi    = 0x3f462e42fec00000; __m256d __dbLn2hi = _mm256_set1_pd(*((double*) &dbLn2hi)); 
	long long dbLn2lo    = 0x3d5d1cf79abc9e3b; __m256d __dbLn2lo = _mm256_set1_pd(*((double*) &dbLn2lo)); 
	//
	int       iAbsMask   = 0x7fffffff        ; __m128  __iAbsMask   = _mm_set1_ps(*((float*) &iAbsMask)); 
	int       iDomainRange = 0x4086232a      ; __m128  __iDomainRange   = _mm_set1_ps(*((float*) &iDomainRange)); 
	__asm__ __volatile__(
			/*"movq %1,%%r10;"*/
			"vmovupd (%%rsi), %%ymm2;"
			"vmovupd %2, %%ymm3;"
			"vmovupd %3, %%ymm1;"
			"vmovupd %4, %%ymm4;"
			/* dM = X*dbInvLn2+dbShifter, dbInvLn2 = 2^k/Ln2 */
			"vfmadd213pd %%ymm1, %%ymm2, %%ymm3;"
			/* iAbsX = (int)(lX>>32), lX = *(longlong*)&X */
			"vextracti128 $1, %%ymm2, %%xmm5;"
			"vshufps   $221, %%xmm5, %%xmm2, %%xmm6;"
			/* iAbsX = iAbsX&iAbsMask */
			"vandps %5, %%xmm6, %%xmm7;"
#if 1
			/* dN = dM-dbShifter, dN = rint(X*2^k/Ln2) */
			"vsubpd    %%ymm1, %%ymm3, %%ymm6;"
			/* iRangeMask = (iAbsX>iDomainRange) */
			"vpcmpgtd %6, %%xmm7, %%xmm0;"
			"vmovupd %10, %%ymm1;"
			"vmovupd %7, %%ymm7;"
			/* Mask = iRangeMask?1:0, set mask for overflow/underflow */
			"vmovmskps %%xmm0, %%ecx;"
			"vmovupd %9, %%ymm0;"
			/* dR = X - dN*dbLn2hi, dbLn2hi is 52-8-k hi bits of ln2/2^k */
			"vmovdqa   %%ymm2, %%ymm5;"
			"vfnmadd231pd %%ymm6, %%ymm1, %%ymm5;"
			/* dR = dR - dN*dbLn2lo, dbLn2lo is 40..94 bits of lo part of ln2/2^k */
			"vfnmadd132pd %11, %%ymm5, %%ymm6;"
			/* exp(r) = b0+r*(b0+r*(b1+r*b2)) */
			"vfmadd213pd %8, %%ymm6, %%ymm0;"
			"vfmadd213pd %%ymm7, %%ymm6, %%ymm0;"
			"vfmadd213pd %%ymm7, %%ymm6, %%ymm0;"
			/* lIndex = (*(longlong*)&dM)&lIndexMask, lIndex is the lower K bits of lM */
			"vandps    %%ymm4, %%ymm3, %%ymm1;"
			/* table lookup for dT[j] = 2^(j/2^k) */
			"vxorpd    %%ymm6, %%ymm6, %%ymm6;"
			/**"vpcmpeqd  %%ymm5, %%ymm5, %%ymm5;"**/
			"vgatherqpd %%ymm5, (%12, %%ymm1, 8), %%ymm6;"
			/* lM = (*(longlong*)&dM)&(~lIndexMask) */
			"vpandn    %%ymm3, %%ymm4, %%ymm3;"
			/* 2^(j/2^k) * exp(r) */
			"vmulpd    %%ymm0, %%ymm6, %%ymm0;"
			/* lM = lM<<(52-K), 2^M */
			"vpsllq    $42, %%ymm3, %%ymm4;"
			/* multiply by 2^M through integer add */
			"vpaddq    %%ymm4, %%ymm0, %%ymm0;"
#endif
			"vmovdqa %%ymm0, %0;"
			:
			:"m" (y), "S" (x), 
			"m" (__dbInvLn2), "m" (__dbShifter), "m" (__lIndexMask), 
			"m" (__iAbsMask), "m" (__iDomainRange), 
			"m" (__dPC0), "m" (__dPC1), "x" (__dPC2),
			"m" (__dbLn2hi), "m" (__dbLn2lo), "r" (&exp_lut)
			: "%ecx" 
			);
}
#endif
