#pragma once
#include <stdio.h>
#include <complex.h>
#include "expf.h"
#include "logf.h"

//
inline static
float marla_powf(float x, float y)
{
	return marla_expf(marla_logf(x)*y);
}
