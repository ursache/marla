#pragma once
#include <stdio.h>
#include "frexp.h"
#include "frexpf.h"
#ifdef __WITH_MPFR
#include <mpfr.h>
#endif

static
void printIEEE754(const double x)
{
	int e;
	double mantissa = marla_frexp(x, &e); 
	printf("%.15f*2^%d\n", mantissa, e);
}



static
void printBits(void const* ptr, size_t const size)
{
        unsigned char *b = (unsigned char*) ptr;
        unsigned char byte;
        int i, j;

        size_t pos = 0;

        for (i=size-1; i>=0 ; i--)
        {
                for (j = 7; j >= 0; j--)
                {
                        byte = (b[i] >> j) & 1;
                        printf("%u", byte);
                        pos++;
                        //if ((pos == 1) || (pos == 12)) printf(" ");
                }
        }
        puts("");
}


static
void printBits64(void const* ptr)
{
        unsigned char *b = (unsigned char*) ptr;
        unsigned char byte;
        int i, j;

        size_t pos = 0;

        for (i = 8 - 1; i >= 0; i--)
        {
                for (j = 7; j >= 0; j--)
                {
                        byte = (b[i] >> j) & 1;
                        printf("%u", byte);
                        pos++;
                        if ((pos == 1) || (pos == 12)) printf(" ");
                }
        }
        puts("");
}


static
void printBits32(void const* ptr)
{
        unsigned char *b = (unsigned char*) ptr;
        unsigned char byte;
        int i, j;

        size_t pos = 0;

        for (i = 4 - 1; i >= 0 ; i--)
        {
                for (j = 7; j >= 0; j--)
                {
                        byte = (b[i] >> j) & 1;
                        printf("%u", byte);
                        pos++;
                        if ((pos == 1) || (pos == 9)) printf(" ");
                }
        }
        puts("");
}

#ifdef __WITH_MPFR

static 
void computeRelativeError(mpfr_t* error, const double* __restrict__ x, const mpfr_t* __restrict__ ymp, const int N)
{
        //mpfr_t* __restrict__ xmp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        mpfr_t xmp;
	mpfr_init2(xmp,    80);
	mpfr_set_d(xmp,    0., MPFR_RNDN);
	mpfr_set_d(*error, 0., MPFR_RNDN);
	//mpfr_out_str (stdout, 10, 0, xmp, MPFR_RNDN); printf("\n"); fflush(stdout);
#if 1
	for (int ii = 0; ii < N; ++ii)
	{
		mpfr_set_d(xmp, x[ii], MPFR_RNDN); 
		mpfr_sub  (xmp, ymp[ii], xmp, MPFR_RNDN);
		mpfr_div  (xmp, xmp, ymp[ii], MPFR_RNDN);
		mpfr_abs  (*error, xmp, MPFR_RNDN); 
		if (mpfr_greater_p(xmp, *error))  mpfr_set(*error, xmp, MPFR_RNDN); 
	}
#endif
}
//
static
void computeRelativeErrorf(mpfr_t* error, const float* __restrict__ x, const mpfr_t* __restrict__ ymp, const int N)
{
        //mpfr_t* __restrict__ xmp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
        mpfr_t xmp;
        mpfr_init2(xmp,    80);
        mpfr_set_d(xmp,    0., MPFR_RNDN);
        mpfr_set_d(*error, 0., MPFR_RNDN);
        //mpfr_out_str (stdout, 10, 0, xmp, MPFR_RNDN); printf("\n"); fflush(stdout);
#if 1
        for (int ii = 0; ii < N; ++ii)
        {
                mpfr_set_d(xmp, x[ii], MPFR_RNDN);
                mpfr_sub  (xmp, ymp[ii], xmp, MPFR_RNDN);
                mpfr_div  (xmp, xmp, ymp[ii], MPFR_RNDN);
                mpfr_abs  (*error, xmp, MPFR_RNDN);
                if (mpfr_greater_p(xmp, *error))  mpfr_set(*error, xmp, MPFR_RNDN);
        }
#endif
}
//
//
//
static
void computeULP(mpfr_t max_ulp, const double* __restrict__ x, const mpfr_t* __restrict__ ymp, int N)
{
        //mpfr_t* __restrict__ xmp  = (mpfr_t*) malloc(sizeof(mpfr_t)*N);
	//mpfr_t max_ulp;
	//mpfr_init2(max_ulp, 80);
	mpfr_set_d(max_ulp, 0., MPFR_RNDN);
	//
	mpfr_t radix; mpfr_init2(radix, 80); 
	mpfr_set_d(radix, 2., MPFR_RNDN);
	//
	mpfr_t mantmp; mpfr_init2(mantmp, 80);
	//
	mpfr_t expmp; mpfr_init2(expmp, 80);
	//
	mpfr_t ze; mpfr_init2(ze, 80);
	//
	mpfr_t ulp; mpfr_init2(ulp, 80);
	//
	mpfr_t twomp; mpfr_init2(twomp, 80);
	mpfr_set_d(twomp, 2., MPFR_RNDN);
	//
	mpfr_t betap; mpfr_init2(betap, 80);
	mpfr_set_d(betap, pow(10., 15), MPFR_RNDN);
	//
	mpfr_t zmp; mpfr_init2(zmp, 80);
	//
        for (int ii = 0; ii < N; ++ii)
        {
		double mantissa; int exp;
		mantissa = marla_frexp(x[ii], &exp);
		mpfr_set_d(mantmp, mantissa, MPFR_RNDN);
		mpfr_set_d(expmp , exp     , MPFR_RNDN);
		mpfr_pow(ze, twomp, expmp, MPFR_RNDN); 
		mpfr_div(ze, ymp[ii],  ze, MPFR_RNDN); 
		mpfr_sub(ulp, mantmp,   ze, MPFR_RNDN); 
		mpfr_abs(ulp,          ulp, MPFR_RNDN);
		mpfr_mul(ulp, ulp, betap, MPFR_RNDN); 
		if (mpfr_greater_p(ulp, max_ulp)) mpfr_set(max_ulp, ulp, MPFR_RNDN);
        }
}

static                                                                                                 
void computeULPf(mpfr_t max_ulp, const float* __restrict__ x, const mpfr_t* __restrict__ ymp, int N)   
{                                                                                                      
	//
        mpfr_set_d(max_ulp, 0., MPFR_RNDN);                                                            
        //                                                                                             
        mpfr_t radix; mpfr_init2(radix, 48);                                                           
        mpfr_set_d(radix, 2., MPFR_RNDN);                                                              
        //                                                                                             
        mpfr_t mantmp; mpfr_init2(mantmp, 48);                                                         
        mpfr_t expmp ; mpfr_init2(expmp , 48);                                                           
        mpfr_t ze    ; mpfr_init2(ze    , 48);                                                                 
        mpfr_t ulp   ; mpfr_init2(ulp, 48);                                                               
        //                                                                                             
        mpfr_t twomp; mpfr_init2(twomp, 48);                                                           
        mpfr_set_d(twomp, 2., MPFR_RNDN);                                                              
        //                                                                                             
        mpfr_t betap; mpfr_init2(betap, 48);                                                           
        mpfr_set_d(betap, pow(10., 8), MPFR_RNDN);                                                    
        //                                                                                             
        mpfr_t zmp; mpfr_init2(zmp, 48);                                                               
        //                                                                                             
        for (int ii = 0; ii < N; ++ii)                                                                 
        {                                                                                              
                float mantissa; int exp;                                                              
                mantissa = marla_frexpf(x[ii], &exp);                                                   
                mpfr_set_d(mantmp, mantissa, MPFR_RNDN);                                               
                mpfr_set_d(expmp , exp     , MPFR_RNDN);                                               
                mpfr_pow(ze, twomp, expmp, MPFR_RNDN);                                                 
                mpfr_div(ze, ymp[ii],  ze, MPFR_RNDN);                                                 
                mpfr_sub(ulp, mantmp,   ze, MPFR_RNDN);                                                
                mpfr_abs(ulp,          ulp, MPFR_RNDN);                                               
                mpfr_mul(ulp, ulp, betap, MPFR_RNDN);                                                  
                if (mpfr_greater_p(ulp, max_ulp)) mpfr_set(max_ulp, ulp, MPFR_RNDN);                   
        }                                                                                              
}                                                                                                      


#endif
