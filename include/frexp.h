#pragma once

//#include "utils.h"
#include <stdint.h>

inline static
double marla_frexp( double x, int* pw2)
{
        uint64_t z = *((uint64_t *) &x);
        const uint32_t pow = ((z >> 52) & 0x7ff) - 1022; 
        z = (z & 0x800fffffffffffff) | 0x3fe0000000000000;
        double man = *(double *) &z;
        *pw2  = pow;
        return man;
}

#if 0
inline
double frexp_old( int* pw2, double* px )
{
	union
	{
		double y;
		unsigned short sh[4];
	} u;
	int i;
	short *q;
	double x = *px;
	u.y = x;

	q = (short *)&u.sh[3];

	/* find the exponent (power of 2) */
	i  = ( *q >> 4) & 0x7ff;
	i    -= 0x3fe;
	*pw2  = i;
	*q   &= 0x800f;
	*q   |= 0x3fe0;
	return( u.y );
}
#endif
