#pragma once
#include "rcp.h"

#ifndef RCP_NR
#define RCP_NR r = r*(2. - a*r)
#endif
#ifndef RCP
#define RCP \
	float fa = (float) a; \
	double r   = 1.f/fa
#endif
#define RCP_NR1 \
	RCP; RCP_NR;
#define RCP_NR2 \
	RCP_NR1; RCP_NR;
#define RCP_NR3 \
	RCP_NR2; RCP_NR;
#define RCP_NR4 \
	RCP_NR3; RCP_NR;

inline
double marla_div(double b, double a)
{
    return b/a;
}



inline
double marla_div_nr0(double b, double a)
{
	return b*marla_rcp(a);
}

inline
double marla_div_nr1(double b, double a)
{
	return b*marla_rcp_nr1(a);
}

inline
double marla_div_nr2(double b, double a)
{
	return b*marla_rcp_nr2(a);
}

inline
double marla_div_nr3(double b, double a)
{
	return b*marla_rcp_nr3(a);
}

inline
double marla_div_nr4(double b, double a)
{
	return b*marla_rcp_nr4(a);
}
