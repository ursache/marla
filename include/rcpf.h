#pragma once

inline
float marla_rcpf(const float* pa)
{
	float fa = *pa;
	float r   = 1.f/fa;
	return r;
}

inline
float marla_rcpf_nr0(const float* pa)
{
        float a = *pa;
	float r   = 1.f/a;
        return r;
}

inline
float marla_rcpf_nr1(const float* pa)
{
        float a = *pa;
        float r   = 1.f/a;
        r = r*(2.f - a*r);
        return r;
}

inline
float marla_rcpf_nr2(const float* pa)
{
        float a = *pa;
        float r   = 1.f/a;
        r = r*(2.f - a*r);
        r = r*(2.f - a*r);
        return r;
}

inline
float marla_rcpf_nr3(const float* pa)
{
        float a = *pa;
        float r   = 1.f/a;
        r = r*(2.f - a*r);
        r = r*(2.f - a*r);
        r = r*(2.f - a*r);
        return r;
}

inline
float marla_rcpf_nr4(const float* pa)
{
        float a = *pa;
        float r   = 1.f/a;
        r = r*(2.f - a*r);
        r = r*(2.f - a*r);
        r = r*(2.f - a*r);
        r = r*(2.f - a*r);
        return r;
}



