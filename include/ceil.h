#pragma once

#include "floor.h"

inline
double marla_ceil(double *x)
{
	double y;

	y = marla_floor(x);
	if( y < *x ) y += 1.0;

	return y;
}

inline
double marla_ceil_overflow(double* x)
{
	double y;

    y = (int) *x - (*x < (int) *x); 
    if( y < *x ) y += 1.0;

    return y;
}
