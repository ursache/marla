#pragma once

#include <stdio.h>
#include <math.h>

#include "floorf.h"
#include "constf.h"
#include "ldexpf.h"
#include "polevlf.h"
#include "types.h"
//
float sincoeff[] = {
 1.58962301576546568060E-10,
-2.50507477628578072866E-8,
 2.75573136213857245213E-6,
-1.98412698295895385996E-4,
 8.33333333332211858878E-3,
-1.66666666666666307295E-1,
};
//
#define pi1  3.141592852771282
#define pi2 -0.000000087422780
#define pi3 -0.000000000000003
//
unsigned int fShift  = 0x4b400000;
//
// this is necessary for the rint trick
//#pragma option_override(marla_sinf,  "opt(level, 2)")
inline
float marla_sinf(float x)
{
	//
	float r  = i32_to_f32((f32_to_i32 (x) &  __fAbsMask));
	unsigned int sign = f32_to_i32 (x) & ~__fAbsMask;
	// n = rint(|x|/pi) using the shift trick 
	float n = INVPIf*r + i32_to_f32(__fShift);
	sign ^= (f32_to_i32(n) << 31);
	// using local fShift to avoid optimization
	n -= i32_to_f32(fShift); 
	// r = |x| - n*pi  [-pi/2, pi/2] range reduction 
	r += -pi1*n;
	r += -pi2*n;
	r += -pi3*n;
	//
	float rr = r*r;
	float y = marla_polevlf( rr, sincoeff, 5);
	//
	y = r*(y*rr + 1.f);
	//
	return i32_to_f32(f32_to_i32(y)^sign);
}
//
/* Degrees, minutes, seconds to radians: */
//
/* 1 arc second, in radians = 4.8481368110953599358991410e-5 */
//
/*
double P64800 = 4.8481368110953599358991410e-5;

double radian(double d, double m, double s)
{

	return( ((d*60.0 + m)*60.0 + s)*P64800 );
}
*/
