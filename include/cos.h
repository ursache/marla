#pragma once

#include <stdio.h>
#include <math.h>

#include "floor.h"
#include "const.h"
#include "ldexp.h"
#include "polevl.h"
#include "div.h"


double sincof[] = {
 1.58962301576546568060E-10,
-2.50507477628578072866E-8,
 2.75573136213857245213E-6,
-1.98412698295895385996E-4,
 8.33333333332211858878E-3,
-1.66666666666666307295E-1,
};
double coscof[] = {
-1.13585365213876817300E-11,
 2.08757008419747316778E-9,
-2.75573141792967388112E-7,
 2.48015872888517045348E-5,
-1.38888888888730564116E-3,
 4.16666666666665929218E-2,
};
double DP1c =   7.85398125648498535156E-1;
double DP2c =   3.77489470793079817668E-8;
double DP3c =   2.69515142907905952645E-15;
//
//double marla_polevl ( double, double *, int );
//double p1evl ( double, void *, int );
//double floor ( double );
//double marla_ldexp ( double, int );
//extern double PIO4;
//extern inline double marla_floor(double);
//
inline
double marla_cos(double x)
{
	double y, z, zz;
	int j, sign;

	//if( x == 0.0 )
	//	return(x);
	/* make argument positive but save the sign */
	sign = 1;
	if( x < 0 )
	{
		x = -x;
	}
	//
	y = x/PIO4;
	//y = marla_floor(y); /* integer part of x/PIO4 */
	y = floor(y); /* integer part of x/PIO4 */
	z = (int) y % 16;
	//
	j = z; /* convert to integer for tests on the phase angle */
	//printf("--> j = %d\n", j);
	/* map zeros to origin */
	if( j & 1 )
	{
		j += 1;
		y += 1.0;
	}
	j = j & 07; /* octant modulo 360 degrees */
	/* reflect in x axis */
	if( j > 3)
	{
		sign = -sign;
		j -= 4;
	}
	if( j > 1)
		sign = -sign; 
	/* Extended precision modular arithmetic */
	z = ((x - y*DP1c) - y*DP2c) - y*DP3c;
	zz  = z*z;
	//
	int  one =  1;
	int mone = -1;
	//
	double cosp = marla_polevl( zz, coscof, 5 );
	double sinp = marla_polevl( zz, sincof, 5 );
	//
	if((j==1) || (j==2))
	{
		//y = 1.0 - marla_ldexp(&zz, &mone) + zz * zz * marla_polevl( zz, &coscof, 5 );
		y = z + z*zz*sinp;
	}
	else
	{
		//y = z + z*zz* marla_polevl( zz, &sincof, 5 );
		y = 1. - zz/2. + zz*zz*cosp;
	}
	//
	if(sign < 0) y = -y;
	return(y);
}
// cody and waite implementation
inline
double marla_cos_CW(double x)
{
	//double y = x/PI + 0.5;
        //y = marla_floor(y);	
	//
		
}
//
/* Degrees, minutes, seconds to radians: */
//
/* 1 arc second, in radians = 4.8481368110953599358991410e-5 */
//
/*
double P64800 = 4.8481368110953599358991410e-5;
 
double radian(double d, double m, double s)
{

        return( ((d*60.0 + m)*60.0 + s)*P64800 );
}
*/
