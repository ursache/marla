#pragma once

//#define exp marla_exp_lut

#include <stdio.h>
#include <complex.h>
#include "sincos.h"
#include "exp.h"
//
inline static
double complex marla_cexp(double complex z)
{
	double x = creal(z);
	double y = cimag(z);
	double r = marla_exp(x);
	double c,s;
	marla_sincos(y, &s, &c);
	return r*c + r*s*I;
}
