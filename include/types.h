#pragma once

//
inline long long f64_to_i64(double x)    { return *( long long *) &x; }
inline double    i64_to_f64(long long i) { return *( double *) &i; }
//
inline int   f32_to_i32(float x) { return *( int *) &x; }
inline float i32_to_f32(int i)   { return *(float *) &i; }
//

