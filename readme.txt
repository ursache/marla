Most Accurate and Rapid Library for Arithmetics: vectorized math functions library
- C header-only library
- implicit vectorization, i.e. no intrinsics involved for portability (tested with gcc 8.1+ and intel 18+)
- single and double precision support
- designed to be used with the "-Ofast" compiler option, no denormal, overflow or underflow support
- tested on x86, arm and risc-v

Based on the books:
"Methods and Programs for Mathematical Functions" by Stephen L. Moshier
ISBN 0133224953 

"Software Manual for the Elementary Functions" by William J. Cody Jr. and William Waite.
ISBN 0138220646

authors:
gilles fourestey (gilles.fourestey@epfl.ch)

"Slide."
